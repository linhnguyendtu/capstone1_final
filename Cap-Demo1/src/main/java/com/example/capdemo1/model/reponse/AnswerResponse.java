package com.example.capdemo1.model.reponse;

import com.example.capdemo1.model.reponse.structure.ListAnswerByQuestionResponse;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AnswerResponse {

    @JsonProperty("question_id")
    private long questionId;

    @JsonProperty("question_content")
    private String questionContent;

    @JsonProperty("answer_content")
    private String answerContent;

    @JsonProperty("create_date")
    private Date createDate;
}


