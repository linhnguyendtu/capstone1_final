import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Teacher } from 'src/app/models/teacher/teacher.model';

@Injectable({
  providedIn: 'root'
})
export class TeacherService {
  teacher: Teacher;
  teacherURL: string = 'http://localhost:8080/api/teacher/teacher-information/6';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }
  getTeacher() {
    return this.http.get(this.teacherURL ,this.httpOptions);
  }
  getTeacherById(id: number) {
    return this.http.get(this.teacherURL + "/" +id,this.httpOptions);
  }
}
