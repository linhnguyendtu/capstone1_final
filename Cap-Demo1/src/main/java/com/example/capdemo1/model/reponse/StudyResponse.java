package com.example.capdemo1.model.reponse;

import com.example.capdemo1.model.reponse.structure.CourseForStudentReponse;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudyResponse {

    @JsonProperty("student_id")
    private String studentId;

    @JsonProperty("student_full_name")
    private String studentFullName;

    @JsonProperty("list_course")
    private List<CourseForStudentReponse> listCourse;
}
