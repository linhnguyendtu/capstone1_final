package com.example.capdemo1.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SMSRequest {
    private static final long serialVersionUID = 1L;

    @JsonProperty("ApiKey")
    private String ApiKey  ;

    @JsonProperty("SecretKey")
    private String SecretKey ;

    @JsonProperty("Content")
    private String Content ;

    @JsonProperty("Phone")
    private String Phone  ;

    @JsonProperty("SmsType")
    private Integer SmsType=8 ;

}
