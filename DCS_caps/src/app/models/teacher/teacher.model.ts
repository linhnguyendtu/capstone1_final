export class Teacher {
    teacher_id: string;
    teacher_full_name: string;
    birthday: string;
    address: string;
    phone: string;
    email: string;
}
