package com.example.capdemo1.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AnswerRequest {

    @JsonProperty("answer_id")
    private long answerId;

    @JsonProperty("question_id")
    private long questionId;

    @JsonProperty("parent_id")
    private String parentId;

    @JsonProperty("class_id")
    private String classId;

    @JsonProperty("answer_content")
    private String answerContent;

    @JsonProperty("create_date")
    private String createDate;

}
