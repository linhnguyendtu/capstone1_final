package com.example.capdemo1.model.reponse.structure;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SubjectForCourseReponse {

    @JsonProperty("subject_name")
    private String subjectName;

    @JsonProperty("out_of_10")
    private String outOf10;

    @JsonProperty("out_of_4")
    private String outOf4;

    @JsonProperty("status")
    private String status;

}
