import { Component, OnInit } from '@angular/core';

import { StudyService } from 'src/app/service/study/study.service';
import { Subject } from 'src/app/models/subject/subject.model';


@Component({
  selector: 'app-study',
  templateUrl: './study.component.html',
  styleUrls: ['./study.component.css']
})
export class StudyComponent implements OnInit {
  public courses: Array<any> = [];
  public subjects: Array<any> = [];
  public gpa: string;
  public selectedValue: string = '';
  studentsName: Array<string> = [];
  //Mảng chứa môn học từng năm
  first: Array<any> = [];
  second: Array<any> = [];
  third: Array<any> = [];
  fourthYear: Array<any> = [];
  //test
  a: number = 10;
  constructor(private StudyService: StudyService) { }

  ngOnInit(): void {
    this.getGPA();
    this.getStudentName();
    this.getScoreByStudentName(this.selectedValue);
  }
  onChange(nameStudent: string) {
    this.selectedValue = nameStudent;
    this.getScoreByStudentName(this.selectedValue);
  }
  getGPA() {
    this.StudyService.getSubjects().subscribe((res:any) => {
      let sumScore = 0;
      let numberOfSubject = 0;
        for(let subject of res) {
          // Môn học chưa học có điểm = 0
          if(subject.out_of_10 === "") subject.out_of_10 = 0;
          subject.out_of_10 = parseInt(subject.out_of_10);
          sumScore += subject.out_of_10;
          numberOfSubject++;
        }
        this.gpa = (sumScore / numberOfSubject).toFixed(1);
    });
    return this.gpa;
  }
  getStudentName() {
    this.StudyService.getSubjects().subscribe((res: any) => {
      for(let student of res) {
        this.studentsName.push(student.student_full_name);
      }
      this.selectedValue = this.studentsName[0];
      this.getScoreByStudentName(this.selectedValue);
    })
  }
  getScoreByStudentName(studentName: string) {
    this.StudyService.getSubjects().subscribe((res: any) => {

      for(let student of res) {

        if(studentName === student.student_full_name) {
          this.courses = student.list_course;
          this.first = this.courses[0].list_subject;
          this.second = this.courses[1].list_subject;

          break;
        }
      }
      //
    })
  }
}

