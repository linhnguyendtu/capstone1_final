package com.example.capdemo1.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "course")
public class CourseEntity {
    @Id
    private String courseId;

    @Column(name = "academy_year")
    private String academyYear;

    @Column(name = "semester")
    private int semester;
}
