package com.example.capdemo1.model.reponse.structure;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ListClassOfTeacher {

    @JsonProperty("class_id")
    private String classId;

    @JsonProperty("class_full_name")
    private String classFullName;

    @JsonProperty("number_of_member")
    private String numberOfMember;

}
