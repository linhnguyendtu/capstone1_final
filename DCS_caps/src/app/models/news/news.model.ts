export class News {
    id: number;
    create_by: string;
    create_date: Date;
    modified_by: string;
    modified_date: Date;
    content: string;
    short_description: string;
    thumbnail: string;
    title: string;
    category_id: number;
}
