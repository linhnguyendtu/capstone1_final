import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ContactComponent} from './layout/parent/contact/contact-dashboard/contact/contact.component';

import {ParentQuestionComponent} from './layout/parent/Question/parent-question/parent-question/parent-question.component';
import {AccountComponent} from './layout/parent/account/account/account.component'
import {StudyComponent} from './layout/parent/study/study/study.component';
import {TuitionDetailComponent} from './layout/parent/tuition/tuition-detail/tuition-detail/tuition-detail.component';
import {ApartmentComponent} from './layout/parent/contact/apartment/apartment/apartment.component'
import {HomeComponent} from './layout/parent/home/home/home.component';
import {LoginComponent} from "./layout/login/login.component";
import {HomeTeacherComponent} from "./layout/teacher/home-teacher/home-teacher.component";
import {ClassComponent} from "./layout/teacher/class/class.component";
import { HomepageParentComponent } from './layout/parent/homepage-parent/homepage-parent/homepage-parent.component';
import {HeaderComponent} from "./layout/teacher/header/header.component";
import { SlidebarComponent } from './layout/teacher/slidebar/slidebar.component';
import { FaqComponent } from './layout/parent/Question/faq/faq/faq.component';
import { TeacherContactComponent } from './layout/parent/contact/teacher/teacher-contact/teacher-contact.component';
import {DashTeacherComponent} from "./layout/teacher/dash-teacher/dash-teacher.component";

const routes: Routes = [
  {
    path: "login",component: LoginComponent
  },

  {
    path: "teacher",component: HomeTeacherComponent,
    children:[
      {path: "class/:id", component: ClassComponent},
      {path: "home",component: DashTeacherComponent}
    ]
  },
  {
  path: "parent", component: HomepageParentComponent,
  children: [
    {path: "home", component: HomeComponent},
    {path: "study", component: StudyComponent},
    {path: "tuition", component: TuitionDetailComponent},
    {path: "account", component: AccountComponent},
    {path: "contact", component: ContactComponent,
      children: [
        {path: "apartment",  component: ApartmentComponent},
        {path: "teacher-information", component: TeacherContactComponent}
      ]
    },
    {
       path: "question",
       component: ParentQuestionComponent
    },
    {
      path: "faq",
      component: FaqComponent
    },
    {
       path: ":id",
       component: AccountComponent
     }

  ]
  },
  {path:"", redirectTo: 'login', pathMatch: 'full'},
  {path:"parent", redirectTo:'home', pathMatch:'full'}

  // {
  //   path: "study",
  //   component: StudyComponent
  // },
  // {
  //   path: "tuition",
  //   component: TuitionDetailComponent,
  //
  // },
  // {
  //   path: "contact",
  //   component: ContactComponent,
  //   children: [
  //     {
  //       path: "apartment",
  //       component: ApartmentComponent
  //     }
  //   ]
  // },
  // {
  //   path: "question",
  //   component: ParentQuestionComponent
  // },
  // {
  //   path: ":id",
  //   component: AccountComponent
  // },
  // {
  //   path: "",
  //   component: HomeComponent
  // },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
