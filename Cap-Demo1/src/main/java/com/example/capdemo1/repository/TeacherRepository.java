package com.example.capdemo1.repository;

import com.example.capdemo1.model.entity.ParentEntity;
import com.example.capdemo1.model.entity.TeacherEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeacherRepository extends JpaRepository <TeacherEntity, String> {
    TeacherEntity findFirstByTeacherId(String teacherId);
}
