package com.example.capdemo1.service;

import com.example.capdemo1.model.entity.*;
import com.example.capdemo1.model.reponse.*;
import com.example.capdemo1.model.reponse.structure.ListClassOfTeacher;
import com.example.capdemo1.model.reponse.structure.StudentOfClassReponse;
import com.example.capdemo1.model.request.SMSRequest;
import com.example.capdemo1.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class TeacherService {
    @Autowired
    ClassRepository classRepository;

    @Autowired
    TeacherRepository teacherRepository;

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    ParentRepository parentRepository;

    @Autowired
    QuestionRepository questionRepository;

    @Autowired
    SchoolRepository schoolRepository;

    @Autowired
    AnswerRepository answerRepository;


    private static final Logger LOGGER = LoggerFactory.getLogger(TeacherService.class);

    public void sendSMS(String classId, SMSRequest smsRequest) {
        LOGGER.info("START sendSMS - sendSMS");
        String ApiKey = "60E0994AC943DB73B255A038E7CE63";
        String SecretKey = "EDC8385F76A996ECD5AAB0C2D31FEF";
        Integer SmsType = 8;
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(List.of(MediaType.APPLICATION_JSON));
        List<PhoneResponse> listPhone = new ArrayList<>();
        Set<String> set = new HashSet<>();
        List<StudentEntity> studentEntities = studentRepository.findAllByClassId(classId);
        studentEntities.forEach(data -> {
            set.add(data.getParentId());
        });
        set.forEach(data1 -> {
            List<ParentEntity> parentEntities = parentRepository.findAllByParentId(data1);
            parentEntities.forEach(data2 -> {
                if (data2.getPhone() != null) {
                    PhoneResponse phoneResponse = new PhoneResponse();
                    phoneResponse.setPhone(String.valueOf(data2.getPhone()));
                    listPhone.add(phoneResponse);
                    listPhone.stream().distinct().collect(Collectors.toList());
                }
            });
        });
        listPhone.forEach(phone -> {
            smsRequest.setPhone(phone.getPhone());
            HttpEntity<SMSRequest> entity = new HttpEntity<>(smsRequest, headers);
            restTemplate.exchange("http://rest.esms.vn/MainService.svc/json/SendMultipleMessage_V4_post_json/",
                    HttpMethod.POST,
                    entity,
                    String.class).getBody();
        });
        LOGGER.info("END sendSMS - sendSMS");
    }

    public List<ClassResponse> getClassByTeacherId(String teacherId) {
        LOGGER.info("START getClassByTeacherId - TeacherService");
        List<ClassResponse> classResponseList = new ArrayList<>();
        try {
            TeacherEntity teacherEntity = teacherRepository.findFirstByTeacherId(teacherId);
            ClassResponse classResponse = new ClassResponse();
            classResponse.setTeacherId(teacherId);
            classResponse.setTeacherName(teacherEntity.getTeacherFullName());
            classResponse.setTeacherBirthday(String.valueOf(teacherEntity.getBirthday()));
            classResponse.setEmail(teacherEntity.getEmail());
            classResponse.setAvatar(teacherEntity.getAvatar());
            List<ListClassOfTeacher> listClassOfTeachers = new ArrayList<>();
            List<ClassEntity> classEntityList = classRepository.findAllByTeacherId(teacherId);
            classEntityList.forEach(data -> {
                ListClassOfTeacher listClassOfTeacherResponse = new ListClassOfTeacher();
                listClassOfTeacherResponse.setClassId(data.getClassId());
                listClassOfTeacherResponse.setClassFullName(data.getClassFullName());
                listClassOfTeacherResponse.setNumberOfMember(data.getNumberOfMembers());
                listClassOfTeachers.add(listClassOfTeacherResponse);
            });
            classResponse.setListClass(listClassOfTeachers);
            classResponseList.add(classResponse);
        } catch (Exception e) {
            LOGGER.error("ERROR:", e);
        }
        LOGGER.info("END getClassByTeacherId - TeacherService");
        return classResponseList;
    }

    public List<ClassStudentResponse> getStudentByClassId(String classId) {
        LOGGER.info("START getStudentByClassId - TeacherService");
        List<ClassStudentResponse> classStudentResponseList = new ArrayList<>();
        try {
            ClassEntity classEntities = classRepository.findFirstByClassId(classId);
            SchoolEntity schoolEntity = schoolRepository.findFirstBySchoolId(classEntities.getSchoolId());
            ClassStudentResponse classStudentResponse = new ClassStudentResponse();
            classStudentResponse.setClassId(classId);
            classStudentResponse.setSchoolName(schoolEntity.getSchoolName());
            classStudentResponse.setClassFullName(classEntities.getClassFullName());
            classStudentResponse.setNumberOfMember(classEntities.getNumberOfMembers());
            List<StudentOfClassReponse> studentOfClassReponseList = new ArrayList<>();
            List<StudentEntity> studentEntities = studentRepository.findAllByClassId(classId);
            studentEntities.forEach(data -> {
                StudentOfClassReponse studentForClassResponse = new StudentOfClassReponse();
                ParentEntity parentEntity = parentRepository.findFirstByParentId(data.getParentId());
                studentForClassResponse.setStudentId(data.getStudentId());
                studentForClassResponse.setStudentName(data.getStudentFullName());
                studentForClassResponse.setStudentBirthday(String.valueOf(data.getBirthday()));
                studentForClassResponse.setStudentEmail(data.getEmail());
                studentForClassResponse.setStudentAddress(data.getAddress());
                studentForClassResponse.setPhone(String.valueOf(data.getPhone()));
                studentForClassResponse.setParentFullName(parentEntity.getParentFullName());
                studentForClassResponse.setPhoneParent(parentEntity.getPhone());
                studentOfClassReponseList.add(studentForClassResponse);
            });
            classStudentResponse.setListStudent(studentOfClassReponseList);
            classStudentResponseList.add(classStudentResponse);

        } catch (Exception e) {
            LOGGER.error("ERROR:", e);
        }
        LOGGER.info("END getClassByTeacherId - TeacherService");
        return classStudentResponseList;
    }

    public List<PhoneResponse> getPhone(String classId) {
        List<PhoneResponse> phoneResponses = new ArrayList<>();
        try {
            List<StudentEntity> studentEntities = studentRepository.findAllByClassId(classId);
            studentEntities.forEach(data -> {
                List<ParentEntity> parentEntities = parentRepository.findAllByParentId(data.getParentId());
                parentEntities.forEach(data1 -> {
                    PhoneResponse phoneResponse = new PhoneResponse();
                    if (data1.getPhone() == null) {
                        phoneResponse.setPhone("Null");
                    }
                    phoneResponse.setPhone(String.valueOf(data1.getPhone()));
                    phoneResponses.add(phoneResponse);
                });
            });
        } catch (Exception e) {
            LOGGER.error("ERROR:", e);
        }
        return phoneResponses.stream().distinct().collect(Collectors.toList());
    }

    public List<QuestionResponse> getQuestionByClassId(String classId) {
        LOGGER.info("START getQuestionByClassId - TeacherService");
        List<QuestionResponse> questionResponseList = new ArrayList<>();
        try {
            List<QuestionEntity> questionEntities = questionRepository.findAllByClassId(classId);
            questionEntities.forEach(data -> {
                if (data.getStatus() == 0) {
                    QuestionResponse questionResponse = new QuestionResponse();
                    questionResponse.setQuestionId(data.getQuestionId());
                    questionResponse.setQuestionContent(data.getQuestionContent());
                    questionResponse.setParentId(data.getParentId());
                    questionResponse.setClassId(data.getClassId());
                    questionResponseList.add(questionResponse);
                }
            });
        } catch (Exception e) {
            LOGGER.error("ERROR: ", e);
        }
        LOGGER.info("END getQuestionByClassId - TeacherService");
        return questionResponseList;

    }

    public List<AnswerResponse> getTeacherAnswerByClassId(String classId) {
        LOGGER.info("START getTeacherAnswerByClassId - TeacherService");
        List<AnswerResponse> answerResponseList = new ArrayList<>();
        try {
            List<QuestionEntity> questionEntities = questionRepository.findAllByClassId(classId);
            questionEntities.forEach(data -> {
                if (data.getStatus() == 1) {
                    AnswerResponse answerResponse = new AnswerResponse();
                    AnswerEntity answerEntity = answerRepository.findFirstByQuestionId(data.getQuestionId());
                    answerResponse.setQuestionId(data.getQuestionId());
                    answerResponse.setQuestionContent(data.getQuestionContent());
                    answerResponse.setAnswerContent(answerEntity.getAnswerContent());
                    answerResponse.setCreateDate(answerEntity.getCreateDate());
                    answerResponseList.add(answerResponse);
                }
            });

        } catch (Exception e) {
            LOGGER.error("ERROR: ", e);
        }
        LOGGER.info("END getTeacherAnswerByClassId - TeacherService");
        return answerResponseList.stream()
                .sorted(Comparator.comparingLong(AnswerResponse::getQuestionId).reversed())
                .collect(Collectors.toList());
    }

    public TeacherResponse getTeacherInformation(String teacherId) {
        LOGGER.info("START getTeacherInformation - TeacherService");
        TeacherResponse teacherResponse = new TeacherResponse();
        try {
            TeacherEntity teacherEntity = teacherRepository.findFirstByTeacherId(teacherId);
            teacherResponse.setTeacherId(teacherEntity.getTeacherId());
            teacherResponse.setTeacherFullName(teacherEntity.getTeacherFullName());
            teacherResponse.setBirthday(String.valueOf(teacherEntity.getBirthday()));
            teacherResponse.setPhone(teacherEntity.getPhone());
            teacherResponse.setEmail(teacherEntity.getEmail());
            teacherResponse.setAddress(teacherEntity.getAddress());
        } catch (Exception e) {
            LOGGER.error("ERROR: ", e);
        }
        LOGGER.info("END getTeacherInformation - TeacherService");
        return teacherResponse;
    }
}






