import { Component, OnInit } from '@angular/core';
import {TokenService} from "../../../../service/token/token.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-homepage-parent',
  templateUrl: './homepage-parent.component.html',
  styleUrls: ['./homepage-parent.component.css']
})
export class HomepageParentComponent implements OnInit {

  constructor(private tokenStorage: TokenService,
              private route: Router) {
  }

  ngOnInit(): void {
    if (this.tokenStorage.getUser().id == null) {
      this.route.navigate(['login']);
    }
  }
}

