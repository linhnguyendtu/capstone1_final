package com.example.capdemo1.model.reponse;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ParentResponse {

    @JsonProperty("parent_id")
    private String parentId;

    @JsonProperty("parent_full_name")
    private String parentFullName;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("address")
    private String address;

    @JsonProperty("avatar")
    private String avatar;

    @JsonProperty("student_id")
    private String studentId;

    @JsonProperty("student_full_name")
    private String studentFullName;

}
