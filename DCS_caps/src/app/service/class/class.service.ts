import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Class} from "../../models/class/class.model";
import {Students} from "../../models/students/students.model";
import {Question} from "../../models/question/question.model";
import {Answer} from "../../models/answer/answer.model";


@Injectable({
  providedIn: 'root'
})
export class ClassService {
  private apiUrlGetClassByTeacherId="http://localhost:8080/api/teacher";
  private apiUrlGetStudentByClassId="http://localhost:8080/api/teacher/class";
  private apiGetQuestion="http://localhost:8080/api/teacher/question";
  private apiPostAnswer="http://localhost:8080/api/question/answer-question";
  private apiGetTeacherAnswer="http://localhost:8080/api/teacher/question-answer";

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private httpClient: HttpClient) {}

  getClassByTeacherId(id: String):Observable<Class[]>{
    return this.httpClient.get<Class[]>(`${this.apiUrlGetClassByTeacherId}/${id}`,this.httpOptions);
  }

  getStudentByClassId(id: any):Observable<Students[]>{
    return this.httpClient.get<Students[]>(`${this.apiUrlGetStudentByClassId}/${id}`);
  }
  getQuestionByClassId(id: any): Observable<Question[]> {
    return this.httpClient.get<Question[]>(`${this.apiGetQuestion}/${id}`);
  }
  updateAnswer(id: any, content: any): Observable<any> {
    return this.httpClient.put(this.apiPostAnswer + '/' + id, content,this.httpOptions)
  }
  getTeacherAnswer(id: any): Observable<Answer[]>{
    return this.httpClient.get<Answer[]>(`${this.apiGetTeacherAnswer}/${id}`,this.httpOptions)
  }
}
