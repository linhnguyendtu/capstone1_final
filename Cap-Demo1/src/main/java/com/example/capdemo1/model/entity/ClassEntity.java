package com.example.capdemo1.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "class")
public class ClassEntity {
    @Id
    @Column(name = "class_id")
    private String classId;

    @Column(name = "teacher_id")
    private String teacherId;

    @Column(name = "student_id")
    private String studentId;

    @Column(name = "school_id")
    private String schoolId;

    @Column(name = "class_full_name")
    private String classFullName;

    @Column(name = "number_of_members")
    private String numberOfMembers;

    @Column(name = "create_at")
    private Date createAt;
}
