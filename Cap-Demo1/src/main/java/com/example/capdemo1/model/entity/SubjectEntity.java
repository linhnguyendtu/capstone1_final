package com.example.capdemo1.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "subject")
public class SubjectEntity {
    @Id
    private String subjectId;

    @Column(name = "student_id")
    private String studentId;

    @Column(name = "course_id")
    private String courseId;

    @Column(name = "subject_name")
    private String subjectName;

    @Column(name = "out_of_10")
    private String outOf10;

    @Column(name = "letter_grande")
    private String letterGrade;

    @Column(name = "out_of_4")
    private String outOf4;

    @Column(name = "status")
    private int status;

    @Column(name = "create_at")
    private String createAt;
}
