package com.example.capdemo1.repository;

import com.example.capdemo1.model.entity.CourseEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseRepository extends JpaRepository <CourseEntity, String> {
    CourseEntity findFirstByCourseId(String courseId);
}
