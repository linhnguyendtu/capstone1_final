package com.example.capdemo1.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "school")
public class SchoolEntity {
    @Id
    @Column(name = "school_id")
    private String schoolId;

    @Column(name = "class_id")
    private String classId;

    @Column(name = "school_name")
    private String schoolName;

    @Column(name = "school_address")
    private int schoolAddress;


}
