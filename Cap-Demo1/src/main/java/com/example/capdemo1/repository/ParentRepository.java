package com.example.capdemo1.repository;

import com.example.capdemo1.model.entity.ParentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ParentRepository extends JpaRepository <ParentEntity, String> {
    ParentEntity findFirstByParentId(String parentId);
    List<ParentEntity> findAllByParentId(String parentId);
}
