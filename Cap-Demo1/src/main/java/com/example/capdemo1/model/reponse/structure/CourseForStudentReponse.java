package com.example.capdemo1.model.reponse.structure;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CourseForStudentReponse {

    @JsonProperty("academy_year")
    private String academyYear;

    @JsonProperty("semester")
    private String semester;

    @JsonProperty("list_subject")
    private List<SubjectForCourseReponse> listSubject;

}
