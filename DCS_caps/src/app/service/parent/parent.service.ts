import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Parent } from 'src/app/models/parent/parent.model';
@Injectable({
  providedIn: 'root'
})
export class ParentService {

  // Thay cái api tk vào đây là nó chạy được này.
  parentUrl = 'http://localhost:8080/api/study/thong-tin-phu-huynh';


  parent: Parent;

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }

  getParent() {
    return this.http.get(this.parentUrl ,this.httpOptions);
  }
  getParentById(id: number) {
    return this.http.get(this.parentUrl + "/" +id,this.httpOptions);
  }
  /** PUT: update the parent on the server */
  updateAccount(id: number, parent: any) {
    return this.http.put(this.parentUrl+"/" +id, parent, this.httpOptions);
  }
}
