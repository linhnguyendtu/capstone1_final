import { Injectable } from '@angular/core';
import { Subject } from 'src/app/models/subject/subject.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { resolveSanitizationFn } from '@angular/compiler/src/render3/view/template';
@Injectable({
  providedIn: 'root'
})
export class StudyService {
  //api to get subject
//  private subjectUrl = 'https://mocki.io/v1/946b94dd-9ee9-46bd-a49c-a4dc0932e656';
  private subjectUrl = 'http://localhost:8080/api/study/bang-diem/7';
  private subjects: Observable<Subject[]>;

  constructor(private http: HttpClient) { }

  getSubjects(): Observable<Subject[]> {
    return this.http.get<Subject[]>(this.subjectUrl );
  }
}
