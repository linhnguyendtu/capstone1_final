import {Component, OnInit} from '@angular/core';
import {ParentService} from 'src/app/service/parent/parent.service';
import {Router} from "@angular/router";
import {TokenService} from "../../../../service/token/token.service";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  parent: any;

  constructor(private parentService: ParentService,
              private tokenService: TokenService,
              private route: Router
  ) {
  }

  ngOnInit(): void {
    this.parent = this.tokenService.getUser().id;
  }

  logout() {
    this.route.navigate(['/login']);
    localStorage.clear();
    sessionStorage.clear();
  }
}
