export class Answer {
  question_id: string;
  question_content: string;
  answer_content: string;
  create_date: string;
}
