package com.example.capdemo1.repository;

import com.example.capdemo1.model.login.ERole;
import com.example.capdemo1.model.login.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}
