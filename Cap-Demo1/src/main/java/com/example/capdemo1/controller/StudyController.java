package com.example.capdemo1.controller;


import com.example.capdemo1.model.reponse.StudyResponse;
import com.example.capdemo1.service.StudyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;

import java.util.List;


@RestController
@RequestMapping("/api/study")
@CrossOrigin("http://localhost:4200")
public class StudyController {

    @Autowired
    public StudyService studyService;

    @GetMapping("/bang-diem/{parentId}")
    public List<StudyResponse> getCoreStudy(@PathVariable(value = "parentId") String parentId){
        return studyService.getCoreStudy(parentId);
    }
}
