package com.example.capdemo1.repository;

import com.example.capdemo1.model.entity.ClassEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ClassRepository extends JpaRepository<ClassEntity, String> {
    ClassEntity findFirstByStudentId(String studentId);
    ClassEntity findFirstByClassId(String classId);

    List<ClassEntity> findAllByTeacherId(String teacherId);
}

