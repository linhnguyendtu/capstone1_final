package com.example.capdemo1.repository;

import com.example.capdemo1.model.entity.SubjectEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SubjectRepository extends JpaRepository <SubjectEntity, String> {

    List<SubjectEntity> findAllByCourseIdAndStudentId(String courseId, String studentId);
    List<SubjectEntity> findAllByStudentId(String studentId);
}
