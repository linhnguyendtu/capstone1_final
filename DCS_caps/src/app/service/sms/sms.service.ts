import {Injectable} from '@angular/core';
import {Sms} from "../../models/sms/sms.model";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SmsService {
  postSendSmsURL: string = "http://localhost:8080/api/teacher/send-sms";
  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private httpClient: HttpClient) {
  }

  postSendSms(id: string, smsContent: Sms): Observable<any> {
    return this.httpClient.post(this.postSendSmsURL + '/' + id, smsContent)

  }
}
