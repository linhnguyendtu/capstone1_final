import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Question} from 'src/app/models/question/question.model';
import {ParentService} from 'src/app/service/parent/parent.service';
import {QuestionService} from 'src/app/service/question/question.service';
import {TokenService} from "../../../../../service/token/token.service";
import {ToastrService} from "ngx-toastr";


@Component({
  selector: 'app-parent-question',
  templateUrl: './parent-question.component.html',
  styleUrls: ['./parent-question.component.css']
})
export class ParentQuestionComponent implements OnInit {
  isClicked = false;
  isClickedAnswer = false;
  isClickedInput = false;
  isOpenFade = false;
  parent_id: string;
  question: Question = new Question();
  list_question: any = [];
  questionToPassForm: any;
  // content for update question
  content: string;
  //Dùng để hiển thị form sửa câu hỏi
  list_update_question = new Array();
  id: number = 0;

  constructor(private questionService: QuestionService,
              private parentService: ParentService,
              private Token: TokenService,
              private Toast: ToastrService) {
  }

  ngOnInit(): void {
    this.parent_id = this.Token.getUser().id;
    this.refreshQuestion();
  }

  refreshQuestion() {
    this.questionService.getQuestion(this.parent_id).subscribe((res) => {
      this.list_question = res;
      console.log('danh sách câu hỏi: ', this.list_question);
      this.list_update_question.length = this.list_question.length;

      this.list_update_question = this.list_update_question.fill('');
    })
  }

  onSubmit(form: NgForm) {
    this.question.question_content = form.value.content;
    this.question.class_id = "CMU-TPM4";
    this.question.parent_id = this.parent_id;

    this.questionService.addQuestion(this.question).subscribe({
      next: data => {
        this.Toast.success("Thêm câu hỏi thành công!!", "Notification")
        this.refreshQuestion();
        form.reset();
      },
      error: error => {
        this.Toast.error("Thêm câu hỏi lỗi!!")
      }
    });
  }

  openQuestionForm() {
    this.isClicked = !this.isClicked;
    console.log('test nefffff', this.isClicked);
  }

  openAnswerView(question: any) {
    this.isClickedAnswer = !this.isClickedAnswer;
    this.isOpenFade = !this.isOpenFade;
    this.questionToPassForm = question;
    this.id = question.question_id;

  }

  updateQuestion(question: any) {
    const body = {
      class_id: question.class_id,
      question_content: this.content,
      create_date: question.create_date,
      list_question: question.list_question,
    };
    this.questionService.updateQuestion(this.id, body).subscribe({
      next: data => {
        this.refreshQuestion();
        this.Toast.success("Sửa câu hỏi thành công!!", "Notification")
      },
      error: error => {
        this.Toast.error("Sửa câu hỏi thất bại");
        console.log(error);
      }
    })
    this.isClickedAnswer = !this.isClickedAnswer;
    this.refreshQuestion();
  }

  deleteQuestion(question: Question) {

    this.questionService.deleteQuestion(question.question_id).subscribe({
      next: data => {
        this.refreshQuestion();
        this.Toast.success("Xóa câu hỏi thành công!!", "Notification")
      },
      error: error => {
        this.Toast.error("Xóa câu hỏi thất bại!!")
      }
    });
  }
}
