import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TuitionDetailComponent } from './tuition-detail.component';

describe('TuitionDetailComponent', () => {
  let component: TuitionDetailComponent;
  let fixture: ComponentFixture<TuitionDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TuitionDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TuitionDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
