import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomepageParentComponent } from './homepage-parent.component';

describe('HomepageParentComponent', () => {
  let component: HomepageParentComponent;
  let fixture: ComponentFixture<HomepageParentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomepageParentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepageParentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
