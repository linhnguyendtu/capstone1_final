import { Component, OnInit } from '@angular/core';
import { Teacher } from 'src/app/models/teacher/teacher.model';
import { TeacherService } from 'src/app/service/teacher/teacher.service';
import { TokenService } from 'src/app/service/token/token.service';

@Component({
  selector: 'app-teacher-contact',
  templateUrl: './teacher-contact.component.html',
  styleUrls: ['./teacher-contact.component.css']
})
export class TeacherContactComponent implements OnInit {
  idTeacher: any;
  teacher: Teacher;
  constructor(private Token: TokenService,
    private TeacherService: TeacherService
    ) { }

  ngOnInit(): void {
    this.getTeacher();
  }
  getTeacher() {
    this.idTeacher = this.Token.getUser().id;
    this.TeacherService.getTeacher().subscribe((res: any) => {
      this.teacher = res;
      console.log("Đây là teacher: ", this.teacher);
    })
  }
}
