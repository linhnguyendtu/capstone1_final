import {Component, OnInit} from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {TokenService} from "../../../service/token/token.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-home-teacher',
  templateUrl: './home-teacher.component.html',
  styleUrls: ['./home-teacher.component.css']
})
export class HomeTeacherComponent implements OnInit {
  classId: string;
  sideBarOpen = true;
  public classId$ = new BehaviorSubject<string>("ClassId");

  constructor(private tokenStorage: TokenService,
              private route: Router) {
  }

  ngOnInit(): void {
    if (this.tokenStorage.getUser().id == null) {
      this.route.navigate(['login']);
    }
  }

  sideBarToggler($event: any) {
    this.sideBarOpen = !this.sideBarOpen;
  }

  onClickGetClassId(event: string) {
    this.classId = event;
    localStorage.setItem('classId', event);
    this.classId$.next(event);

    // console.log("Event: "+event);
    // console.log("classId:"+this.classId)
    // console.log("classId$"+this.classId$);
  }


}
