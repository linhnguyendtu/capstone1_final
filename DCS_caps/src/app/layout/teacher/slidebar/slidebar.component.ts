import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ClassService} from "../../../service/class/class.service";
import {Class} from "../../../models/class/class.model";
import {Router} from '@angular/router';
import {TokenService} from "../../../service/token/token.service";

@Component({
  selector: 'app-slidebar',
  templateUrl: './slidebar.component.html',
  styleUrls: ['./slidebar.component.css']
})
export class SlidebarComponent implements OnInit {
  @Output() buttonClicked: EventEmitter<string> = new EventEmitter<string>();
  Class: Class[];
  teacher: any;
  id: any;
  teacherId: String;
  classOfTeacher: Array<string> = [];

  constructor(private classService: ClassService,
              private router: Router,
              private tokenService: TokenService) {
  }

  ngOnInit(): void {
    this.getClassByTeacherId();
  }

  private getClassByTeacherId() {
    this.teacherId = this.tokenService.getUser().id;
    console.log(this.teacherId);
    this.classService.getClassByTeacherId(this.teacherId).subscribe(data => {
      this.Class = data;
      this.teacher = this.Class[0];
      for (let listClass of this.teacher.list_class_of_teacher) {
        this.classOfTeacher.push(listClass.class_id);
      }
    })
  }

  onClickGetClassId(classId: string) {
    this.id = classId;
    this.buttonClicked.emit(classId);
  }
}
