package com.example.capdemo1.repository;

import com.example.capdemo1.model.entity.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StudentRepository extends JpaRepository <StudentEntity, String> {
    StudentEntity findFirstByParentId(String parentId);
    List<StudentEntity> findAllByParentId(String parentId);
    List<StudentEntity> findAllByClassId(String classId);
}
