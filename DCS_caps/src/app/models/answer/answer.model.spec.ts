import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnswerModel } from './answer.model';

describe('AnswerModel', () => {
  let component: AnswerModel;
  let fixture: ComponentFixture<AnswerModel>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnswerModel ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnswerModel);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
