import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ClassService} from "../../../service/class/class.service";
import {Students} from "../../../models/students/students.model";
import {ActivatedRoute} from "@angular/router";
import {HomeTeacherComponent} from "../home-teacher/home-teacher.component";
import {Sms} from "../../../models/sms/sms.model";
import {SmsService} from "../../../service/sms/sms.service";
import {NgForm} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import {ParentService} from "../../../service/parent/parent.service";
import {Question} from "../../../models/question/question.model";
import {Answer} from "../../../models/answer/answer.model";

@Component({
  selector: 'app-class',
  templateUrl: './class.component.html',
  styleUrls: ['./class.component.css']
})
export class ClassComponent implements OnInit, OnChanges {
  errorNotification: String = "Bạn phải nhập nội dung";
  isOpenError: boolean = false;
  Student: any;
  Answer: Answer[];
  id: any;
  questionId: any;
  listStudent = [];
  listQuestion: any = [];
  sms: Sms = new Sms();
  content: string;
  answerContent: string;
  parentId: any;
  isClickedOpenForm: boolean = false;
  list_answer = new Array();

  constructor(private classService: ClassService,
              private router: ActivatedRoute,
              private home: HomeTeacherComponent,
              private smsService: SmsService,
              private toastService: ToastrService,
              private parentService: ParentService
  ) {
  }

  ngOnInit(): void {
    this.Student = new Students();
    if (localStorage.getItem('classId')) {
      this.home.classId$.subscribe((classId) => {
        this.id = classId;
        this.classService.getStudentByClassId(localStorage.getItem('classId')).subscribe(data => {
          this.Student = data[0];
          console.log(this.Student);
        })
      })
    }
    this.getQuestion();
    this.getTeacherAnswers();
    this.parentService.getParent().subscribe((res: any) => this.parentId = res.parent_id);
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
  }

  openForm() {
    this.isClickedOpenForm = !this.isClickedOpenForm;
  }

  getID(question: Question) {
    this.questionId = question.question_id;
  }

  // Gửi tin nhắn
  onClickSubmitSMS(smsForm: NgForm) {
    this.sms.ApiKey = "60E0994AC943DB73B255A038E7CE63";
    this.sms.SecretKey = "EDC8385F76A996ECD5AAB0C2D31FEF";
    this.sms.SmsType = "8";
    this.sms.Content = this.content;
    if(this.content != null){
      this.smsService.postSendSms(this.id, this.sms).subscribe({
        next: data => {
          this.toastService.success("Gửi tin nhắn thành công!!", "Notification");
          smsForm.reset();
        }
      });
    }
    else {
        this.toastService.error("Gửi tin nhắn không thành công!!", "Warning!!!");
    }


    // this.home.classId$.subscribe((classId) => {
    //   this.id = localStorage.getItem('classId');
    //   this.smsService.postSendSms(this.id, this.sms).subscribe({
    //     next: data => {
    //       this.toastService.success("Gửi tin nhắn thành công!!", "Notification");
    //       smsForm.reset();
    //     },
    //     error: error => {
    //       this.toastService.error("Gửi tin nhắn không thành công!!", "Warning!!!");
    //     }
    //   });
    // });
  }

  // Lấy câu hỏi hiện thị lên UI
  getQuestion() {
    this.home.classId$.subscribe((classId) => {
      this.id = localStorage.getItem('classId');
      console.log(this.id);
      this.classService.getQuestionByClassId(this.id).subscribe({
        next: data => {
          this.listQuestion = data;
          this.list_answer.length = this.listQuestion.length;
          this.list_answer = this.list_answer.fill('');

        },
        error: error => {
          alert("Lỗi rồi lại đi bạn ơi!!!")
        }
      });
    });
  }

  //Gửi câu trả lời
  sendAnswer(questionForm: NgForm) {
    let answer = this.answerContent;
    let classID = this.id;
    let parrentID;
    let i = 0;
    answer = this.list_answer[i];
    let body = {
      parent_id: this.parentId,
      class_id: classID,
      answer_content: answer
    }
    if(answer === null || answer === '') {
      this.toastService.error("Bạn phải nhập câu trả lời", "ERROR")
    }
    else {
      this.classService.updateAnswer(this.questionId, body).subscribe({
        next: data => {
          this.toastService.success("Trả lời câu hỏi thành công", "Notification");
          this.getQuestion();
          this.getTeacherAnswers();
        },
        error: error => {
          this.toastService.error("Trả lời lỗi", "ERROR")
        }
      });
    }
  }

  getTeacherAnswers() {
    this.home.classId$.subscribe((classId) => {
      this.id = localStorage.getItem('classId');
      this.classService.getTeacherAnswer(this.id).subscribe(data => {
        this.Answer = data;
      });
    });
  }

}
