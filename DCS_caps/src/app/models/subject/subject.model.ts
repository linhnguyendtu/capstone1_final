export class Subject {
    name: string;
    out_of_4: number;
    out_of_10: number;
    academic_year: string;
    semester: string;
    studentFullName: string;
    status: string;
}
