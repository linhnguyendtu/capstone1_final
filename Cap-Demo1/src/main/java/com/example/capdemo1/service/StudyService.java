package com.example.capdemo1.service;

import com.example.capdemo1.common.Enums;
import com.example.capdemo1.model.entity.CourseEntity;
import com.example.capdemo1.model.entity.StudentEntity;
import com.example.capdemo1.model.entity.SubjectEntity;
import com.example.capdemo1.model.reponse.StudyResponse;
import com.example.capdemo1.model.reponse.structure.CourseForStudentReponse;
import com.example.capdemo1.model.reponse.structure.SubjectForCourseReponse;
import com.example.capdemo1.repository.CourseRepository;
import com.example.capdemo1.repository.ParentRepository;
import com.example.capdemo1.repository.StudentRepository;
import com.example.capdemo1.repository.SubjectRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class StudyService {
    @Autowired
    public ParentRepository parentRepository;

    @Autowired
    public StudentRepository studentRepository;

    @Autowired
    public CourseRepository courseRepository;

    @Autowired
    public SubjectRepository subjectRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(StudyService.class);

    public List<StudyResponse> getCoreStudy(String parentId) {
        LOGGER.info("START getCoreStudy - StudyService");
        List<StudyResponse> studyResponseList = new ArrayList<>();
        try {
            List<StudentEntity> studentEntity = studentRepository.findAllByParentId(parentId);

            studentEntity.forEach(student -> {
                StudyResponse studyResponse = new StudyResponse();
                studyResponse.setStudentId(student.getStudentId());
                studyResponse.setStudentFullName(student.getStudentFullName());

                List<SubjectEntity> subjectEntityList = subjectRepository.findAllByStudentId(student.getStudentId());
                Set<String> set = new HashSet<>();
                subjectEntityList.forEach(data -> {
                    set.add(data.getCourseId());
                });
                List<CourseForStudentReponse> courseForStudentResponseList = new ArrayList<>();
                set.forEach(data -> {
                    List<SubjectEntity> subjectEntity = subjectRepository.findAllByCourseIdAndStudentId(data, student.getStudentId());
                    List<SubjectForCourseReponse> subjectForCourseResponseList = new ArrayList<>();
                    subjectEntity.forEach(subject -> {
                        SubjectForCourseReponse subjectForCourseReponse = new SubjectForCourseReponse();
                        subjectForCourseReponse.setStatus(Enums.SubjectStatus.findByCode(subject.getStatus()).getValue());
                        subjectForCourseReponse.setSubjectName(subject.getSubjectName());
                        subjectForCourseReponse.setOutOf10(subject.getOutOf10());
                        subjectForCourseReponse.setOutOf4(subject.getOutOf4());
                        subjectForCourseResponseList.add(subjectForCourseReponse);
                    });
                    CourseEntity courseEntity = courseRepository.findFirstByCourseId(data);
                    CourseForStudentReponse courseForStudentReponse = new CourseForStudentReponse();
                    courseForStudentReponse.setSemester(Enums.Semester.findByCode(courseEntity.getSemester()).getValue());
                    courseForStudentReponse.setAcademyYear(courseEntity.getAcademyYear());
                    courseForStudentReponse.setListSubject(subjectForCourseResponseList);
                    courseForStudentResponseList.add(courseForStudentReponse);
                });
                studyResponse.setListCourse(courseForStudentResponseList.stream()
                        .sorted(Comparator.comparing(CourseForStudentReponse::getSemester)).collect(Collectors.toList()));
                studyResponseList.add(studyResponse);
            });

        } catch (Exception e) {
            System.out.println("Something went wrong.");
        }
        LOGGER.info("END getCoreStudy - StudyService");
        return studyResponseList;
    }
}





