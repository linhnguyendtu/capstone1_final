package com.example.capdemo1.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "parent")
public class ParentEntity {
    @Id
    private String parentId;

    @Column(name = "parent_full_name")
    private String parentFullName;

    @Column(name = "birthday")
    private Date birthday;

    @Column(name = "phone")
    private String phone;

    @Column(name = "account")
    private String account;

    @Column(name = "pass")
    private String pass;

    @Column(name = "address")
    private String address;

    @Column(name="avatar")
    private String avatar;

    @Column(name = "role")
    private int role;

    @Column(name = "create_at")
    private Date createAt;

}
