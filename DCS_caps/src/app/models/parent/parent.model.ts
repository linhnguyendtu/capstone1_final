export class Parent {
    parent_id: string;
    parent_full_name: string;
    phone: string;
    address: string;
    avatar: null;
    student_id: string;
    student_full_name: string;
}
