package com.example.capdemo1.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ParentRequest {

    @JsonProperty("parent_full_name")
    private String parentFullName;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("pass")
    private String pass;

    @JsonProperty("address")
    private String address;

    @JsonProperty("avatar")
    private String avatar;

}
