import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Tuition } from 'src/app/models/tuition/tuition.model';

@Injectable({
  providedIn: 'root'
})

export class TuitionService {
  private tuitionUrl = 'http://localhost:8080/api/study/hoc-phi';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  private tuitionObjs: Observable<Tuition>;
  constructor(private http: HttpClient) { }
  getTuition(id: string): Observable<Tuition[]> {
    return this.http.get<Tuition[]>(`${this.tuitionUrl}/${id}`,this.httpOptions);
  }
}
